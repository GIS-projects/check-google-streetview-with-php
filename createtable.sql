CREATE TABLE `StreetViewData` (
  `fid` int(11) NOT NULL DEFAULT '0',
  `x` double NOT NULL DEFAULT '0',
  `y` double NOT NULL DEFAULT '0',
  `xgoogle` double NOT NULL DEFAULT '0',
  `ygoogle` double NOT NULL DEFAULT '0',
  `lastcheck` mediumtext,
  `datestreetview` mediumtext,
  `municipality` varchar(50)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `StreetViewData`
  ADD PRIMARY KEY (`fid`),
  ADD UNIQUE KEY `fid` (`fid`);