<?php
//// DB settings ////
$DB_HOST="localhost"; // Your MySQL host
$DB_NAME="mydatabase"; // Your MySQL database
$DB_USER="myusername"; // Your MySQL username
$DB_PASS="mypassword"; // Your MySQL password
$DB_TABLE="StreetViewData"; // Your MySQL table
/////////////////////////
////////  Connect to MySQL database
$mysqli = mysqli_connect($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
////////  Query the data (and remove doubles and points with no images)
$sql = 'SELECT min(fid) as fid, xgoogle, ygoogle, municipality, datestreetview FROM '.$DB_TABLE.' WHERE xgoogle!=0 AND ygoogle!=0 GROUP BY xgoogle, ygoogle, datestreetview, municipality';
$result = $mysqli->query($sql);
////////  Build GeoJSON feature collection array
$geojson = array(
   'type'      => 'FeatureCollection',
   'features'  => array()
);
////////  Loop through rows to build feature arrays
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    $properties = $row;
    ////////  add year to the properties   
    $properties['yearstreetview'] = strval(substr($properties['datestreetview'], 0, 4));
    ////////  Rename properties
    $properties['x'] = $properties['xgoogle'];
    $properties['y'] = $properties['ygoogle'];
    unset($properties['xgoogle']);
    unset($properties['ygoogle']);
    ////////  Add geometry for the point
    $feature = array(
        'type' => 'Feature',
        'geometry' => array(
            'type' => 'Point',
            'coordinates' => array(
                $row['xgoogle'],
                $row['ygoogle']
            )
        ),
        'properties' => $properties
    );
    ////////  Add feature arrays to feature collection array 
    array_push($geojson['features'], $feature);
}
/// Display GeoJSON
header('Content-type: application/json');
echo json_encode($geojson, JSON_NUMERIC_CHECK);
//// Close database connection /////
mysqli_close($mysqli);
?>