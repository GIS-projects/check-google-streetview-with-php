# Scripts to check Google Streetview dates with PHP

These scripts can be used to check when the most recent Google Streetview images were captured in a certain area.

More information about these scripts can be found on https://stuyts.xyz/2020/02/27/a-map-with-the-most-recent-streetview-capture-dates-per-street-the-php-and-sql-scripts-i-use/.  

* createtable.sql: MySQL script to create an empty table to store the checks in
* check.php: PHP script to check when Streetview Images were last taken for a certain location
* geojson.php: PHP script to convert the MySQL data into a GeoJSON file

Because a Google API key is personal and linked to a credit card, I replaced my API key by _MyApiKey_, but if you want to use these scripts, you’ll have to replace that with your own API key.
