<?php
//// DB settings ////
$DB_HOST="localhost"; // Your MySQL host
$DB_NAME="mydatabase"; // Your MySQL database
$DB_USER="myusername"; // Your MySQL username
$DB_PASS="mypassword"; // Your MySQL password
$DB_TABLE="StreetViewData"; // Your MySQL table
/////////////////////////

//// Set number of checks ////
$numberofchecks=10;
//////////////////////////////

///// Get locations to check from MySQL database /////
$mysqli = mysqli_connect($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
$requestquery = "SELECT * FROM " . $DB_TABLE . " ORDER BY lastcheck LIMIT ".$numberofchecks;
$result = $mysqli->query($requestquery);
/////////////////////////////////////////////

///// Check each location //////
while ($row = mysqli_fetch_array($result)) {
    $fid = urlencode($row["fid"]);
    $x = urlencode($row["x"]);
    $y = urlencode($row["y"]);
    $json = "https://maps.googleapis.com/maps/api/streetview/metadata?output=xml&location=$y,$x&key=MyApiKey";
    $streetview=(json_decode(file_get_contents($json)));
    $datestreetview = $streetview->date;
    $xgoogle = $streetview->location->lng;
    $ygoogle = $streetview->location->lat;
    $now   = date("Ymdhis");
    if ($datestreetview == '') {
        $datestreetview = 'no images';
        $xgoogle = 0;
        $ygoogle = 0;
    }
    echo("<strong>Checked location id: ".$fid."</strong><br />");
    echo("Checked locatien: $x - $y<br />");
    echo("Date of Streetview Image: $datestreetview<br />");
    echo("Streetview Image found at $xgoogle - $ygoogle<br />");
    echo("<hr />");
////////////////////////////////////

    ///// Update info for each location in MySQL table //////
    $updatequery = $mysqli->prepare("UPDATE " . $DB_TABLE . " SET `lastcheck`='" . $now . "', `datestreetview`=?, `xgoogle`=?, `ygoogle`=? WHERE `fid`=?");
    $updatequery->bind_param("ssss",$datestreetview, $xgoogle, $ygoogle, $fid);

    if (!$updatequery->execute()) {
        printf("Errormessage: %s\n", $updatequery->error);
    }

    $counter++;
    //////////////////////////////////////////////////////////
 
}


//// Close database connection /////
mysqli_close($mysqli);
?> 
